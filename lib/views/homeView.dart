import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:flutter/material.dart';

import 'detailView.dart';

/// Created by Anu Alias on 10-03-2021.

class HomePageView extends StatefulWidget {
  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView> with  SingleTickerProviderStateMixin {

  var pi = 3.14;

  TabController tabController;
  int currentIndex =0;



  void _incrementTab(index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SizedBox(
        height: 70,
        child: BottomNavigationBar(
          currentIndex: currentIndex,
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Colors.black,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle_outlined,size: 30,),
              title: Text(""),
            ),
          ],
          onTap: (index){
            _incrementTab(index);
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: SizedBox(
        height: 70,
        child: new FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed:(){

          },
          tooltip: 'Increment',
          child: new Icon(Icons.shopping_bag_outlined,size: 30,),
        ),
      ),
      body:  Stack(
          children: [
            Column(children: [
            Padding(
              padding: const EdgeInsets.only(top:25.0,left:10),
              child: ListTile(
                title: Text("Top Rated",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Inter',
                  fontSize: 35
                ),),
                trailing: Transform.rotate(
                  angle: pi / 2.0,
                    child: Icon(Icons.tune_sharp,color: Colors.black,)
                ),
              ),
            ),
            SizedBox(height: 30,),
            Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Container(
              width :100,
              decoration :new BoxDecoration(
              // color: Colors.grey[200],
              borderRadius:
              new BorderRadius.all(const Radius.circular(30.0))),
              constraints :BoxConstraints.expand(height :50, width :MediaQuery.of(context).size.width),
              child: TabBar(
              controller :tabController,
                labelPadding: EdgeInsets.only(left: 20, right: 20),
              tabs :[
              Container(
              height: 60,
              child:
              Row(
                children: [
                  Icon(Icons.weekend_outlined),SizedBox(width:10),
                  Text("Armchair", style :TextStyle(fontSize :18)),
                ],
              )),

              Container(
                height: 60,
                child:
                Row(
                children: [
                Icon(Icons.king_bed_outlined),SizedBox(width:10),
                Text("Bed", style :TextStyle(fontSize :18)),
                ],
                )),
                Container(
                    height: 60,
                    child:
                    Row(
                      children: [
                        Icon(Icons.lightbulb_outline_rounded),SizedBox(width:10),
                        Text("Lamp", style :TextStyle(fontSize :18)),
                      ],
                    )),
              ],
              indicatorSize :TabBarIndicatorSize.tab,
              indicator: RectangularIndicator(
                color: Colors.black,
                topLeftRadius: 10,
                topRightRadius: 10,
                bottomLeftRadius: 10,
                bottomRightRadius: 10,
                paintingStyle: PaintingStyle.fill,
                ),
              labelColor : Colors.white,
              isScrollable :true,
              unselectedLabelColor: Colors.grey,
              indicatorWeight: 5,
              ),
              ),
            ),
            ),

            Expanded(
            child: TabBarView(
            controller :tabController,
            children :[
              HistoryClass(),
              HistoryClass(),
              HistoryClass(),
            ]),
            )
            ],),
    ],),
    );
  }
}

class HistoryClass extends StatefulWidget {

  @override
  _HistoryClassState createState() => _HistoryClassState();
}

class _HistoryClassState extends State<HistoryClass> {
  List games = [
    "Trending",
    "Most Popular",
  ];

  List images = [
    "images/chair1.png",
    "images/chair2.png",
    "images/chair3.png",
    "images/chair4.png",
  ];
  List chairNames = [
    "Tortor Chair",
    "Morbi Chair",
    "Pretium Chair",
    "Blandit Chair"
  ];
  List chairRates = [
    "4.5",
    "4.8",
    "4.3",
    "4.1"
  ];
  List chairPrice =[
    "256.00",
    "284.00",
    "232.00",
    "224.00"
  ];

  var selectType = "Most Popular";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:20.0,left:25),
                  child: Text("147 Products",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                  ),),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: DropdownButtonHideUnderline(
                    child: new DropdownButton<String>(
                        isExpanded: false,
                        iconSize: 30,
                        iconEnabledColor: Colors.black,
                        items:games.map((val){
                          return DropdownMenuItem<String>(
                            value: val,
                            child: new Text(val),
                          );
                        }).toList(),
                        hint:Text(selectType,
                         style: TextStyle(
                           fontWeight: FontWeight.bold,
                           color: Colors.black
                         ),),
                        onChanged:(String val){
                          selectType= val;
                          setState(() {});
                        }),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: MediaQuery.of(context).size.height* 0.6,
              child: GridView.count(
                  crossAxisCount: 2,
                childAspectRatio: 0.8,
                children: List.generate(4, (index){
                  return Padding(
                    padding: const EdgeInsets.only(left: 10.0,right: 10,bottom: 10),
                    child: GestureDetector(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height * 0.35,
                        width: MediaQuery.of(context).size.width/1.8,
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.all(Radius.circular(60)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Align(
                                    alignment: Alignment.center,
                                    child: Image.asset(images[index],width: 100,)
                                ),
                                SizedBox(height: 20,),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Text(chairNames[index],
                                        style: TextStyle(
                                            fontSize: 13,
                                            fontFamily: 'Poppins'
                                        ),),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 18.0,right: 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(Icons.attach_money,color: Colors.yellow,),
                                          Text(chairPrice[index],
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Poppins'
                                            ),),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.star,color: Colors.yellow,),
                                          Text(chairRates[index],
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Poppins'
                                            ),),
                                        ],
                                      ),

                                    ],
                                  ),
                                )
                              ],
                            )
                        ),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => DetailView()
                        ));
                      },
                    ),
                  );
                })
              ),
            )
          ],
      ),
    );
  }
}

