import 'package:flutter/material.dart';


/// Created by Anu Alias on 10-03-2021.

class DetailView extends StatefulWidget {
  @override
  _DetailViewState createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView> {

  int currentIndex =0;


  void _incrementTab(index) {
    setState(() {
      currentIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SizedBox(
        height: 70,
        child: BottomNavigationBar(
          currentIndex: currentIndex,
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Colors.black,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings,size: 30,),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined,size: 30,),
              title: Text(""),
            ),
          ],
          onTap: (index){
            _incrementTab(index);
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: SizedBox(
        height: 70,
        child: new FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed:(){

          },
          tooltip: 'Increment',
          child: new Icon(Icons.shopping_bag_outlined,size: 30,),
        ),
      ),
      body: Stack(
        children: [
          new Container(
            height: MediaQuery.of(context).size.height * 0.9,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/chair_B.jpg"),
                fit: BoxFit.fill
              ),
              borderRadius: BorderRadius.only(
                bottomRight:Radius.circular(40),
                bottomLeft: Radius.circular(40),
              ),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.only(bottom: 30.0,left:20,right:20),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 130,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 180),
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: ListTile(
                    leading: Image.asset("images/chair_B.png"),
                    title: Text("Elementum Chair",
                     style: TextStyle(
                       fontFamily: "Poppins",
                       fontSize: 16,
                       color: Colors.white,
                       fontWeight: FontWeight.w500
                     ),),
                    subtitle: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.star,color: Colors.yellow,),
                              Text("4.6",
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Poppins',
                                  color: Colors.white,
                                ),),
                              ]),
                              SizedBox(width: 20,),
                              Row(
                                children: [
                                  Icon(Icons.attach_money,color: Colors.yellow,),
                                  Text("224.00",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Poppins',
                                      color: Colors.white
                                    ),),
                                ],
                              ),
                            ],
                          )
                    ),
                    trailing: new CircleAvatar(
                      radius: 30,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.arrow_forward_ios_outlined,color: Colors.black,),
                    ),
                  ),
                )
              ),
            ),
          ),
          new Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top:25.0,left:10),
                child: ListTile(
                  leading: Icon(Icons.camera_alt_outlined,color: Colors.white,),
                  title: Text("Point your camera at a furniture",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Poppins',
                      fontSize: 16,
                      color: Colors.white,
                    ),),
                  trailing: CircleAvatar(
                    backgroundColor: Color.fromRGBO(255, 255, 255, 180),
                    child: Icon(Icons.circle,color: Colors.white,size: 17,),
                  )
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
              Container(
                width: MediaQuery.of(context).size.width /1.2,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 180),
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 35,
                    backgroundColor: Colors.white,
                    child: Icon(Icons.clear,color: Colors.black,size: 25,),
                  ),
                  title: Text(
                    "Elementum Chair: 88.47%",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Poppins',
                      fontSize: 16,
                      color: Colors.white,
                    ),),
                  ),
                ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: CircleAvatar(
                      radius: 15,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.add,color: Colors.black,size: 25,),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: CircleAvatar(
                      radius: 15,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.add,color: Colors.black,size: 25,),
                    ),
                  ),
                ],
              ),

            ],
          )
        ],
      ),
    );
  }
}
